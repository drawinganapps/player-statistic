import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:fifa_worldcup_qatar/helper/color_helper.dart';
import 'package:fifa_worldcup_qatar/pages/player_page.dart';
import 'package:fifa_worldcup_qatar/widgets/bottom_navigation.dart';

class PlayerScreen extends StatelessWidget {
  const PlayerScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorHelper.white,
        leading: Container(
            child: Center(
              child: Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: ColorHelper.grey, width: 1)
                ),
                child: Icon(Icons.arrow_back_ios_new, color: ColorHelper.dark, size: 20,),
              ),
            )),
        title: Text('ARGENTINA', style: GoogleFonts.alfaSlabOne(
          color: ColorHelper.dark,
          fontSize: 20,
          letterSpacing: 1.5
        )),
        centerTitle: true,
      ),
      body: const PlayerPages(),
      bottomNavigationBar: BottomNavigation(),
    );
  }

}
