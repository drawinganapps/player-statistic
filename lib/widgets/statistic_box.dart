import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:fifa_worldcup_qatar/helper/color_helper.dart';

class StatisticBox extends StatelessWidget {
  final String header;
  final String value;
  const StatisticBox({super.key, required this.header, required this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110,
      width: 110,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        color: ColorHelper.grey.withOpacity(0.2)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(header, style: GoogleFonts.alfaSlabOne(
                  fontSize: 15,
                  color: ColorHelper.white
              )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(value, textAlign: TextAlign.end,style: GoogleFonts.alfaSlabOne(
                  fontSize: 35,
                  color: ColorHelper.white
              ))
            ],
          )
        ],
      ),
    );
  }

}
