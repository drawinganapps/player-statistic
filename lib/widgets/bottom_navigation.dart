import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:fifa_worldcup_qatar/helper/color_helper.dart';

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ColorHelper.dark,
      ),
      height: Get.height * 0.085,
      padding: EdgeInsets.only(
          top: Get.height * 0.01,
          left: Get.width * 0.03,
          right: Get.width * 0.03,
          bottom: Get.height * 0.01),
      child: Container(
        decoration: BoxDecoration(
            color: ColorHelper.primary,
            borderRadius: BorderRadius.circular(35)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('FULL BIO',
                style: GoogleFonts.alfaSlabOne(
                    color: ColorHelper.white, fontSize: 16)),
            Container(
              margin: EdgeInsets.only(left: Get.width * 0.02),
              child: Icon(Icons.arrow_forward_ios,
                  color: ColorHelper.white, size: 15),
            )
          ],
        ),
      ),
    );
  }
}
