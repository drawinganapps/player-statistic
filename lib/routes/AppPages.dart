import 'package:fifa_worldcup_qatar/screens/player_screen.dart';
import 'package:fifa_worldcup_qatar/screens/welcome_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.WELCOME,
        page: () => const WelcomeScreen()),
    GetPage(
        name: AppRoutes.PLAYER_DETAIL,
        page: () => const PlayerScreen()),
  ];
}
