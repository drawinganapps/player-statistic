import 'package:flutter/material.dart';

class ColorHelper {
  static Color primary = const Color.fromRGBO(231, 36, 58, 1);
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color grey = const Color.fromRGBO(222, 222, 222, 1);
  static Color dark = const Color.fromRGBO(0, 0, 0, 1);
  static Color yellow = const Color.fromRGBO(246, 186, 61, 1);
  static Color green = const Color.fromRGBO(15, 193, 170, 1);
}
