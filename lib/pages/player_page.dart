import 'package:fifa_worldcup_qatar/helper/color_helper.dart';
import 'package:fifa_worldcup_qatar/widgets/statistic_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class PlayerPages extends StatelessWidget {
  const PlayerPages({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Container(
              height: Get.height * 0.55,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/messi.png'),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.white.withOpacity(0.6), BlendMode.srcOver),
                ),
              ),
              child: Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RotatedBox(
                          quarterTurns: 3,
                          child: Text('MESSI',
                              style: GoogleFonts.alfaSlabOne(
                                  color: ColorHelper.primary,
                                  fontSize: 45,
                                  letterSpacing: 2))),
                      Container(
                        margin: EdgeInsets.only(top: Get.height * 0.03),
                      ),
                      RotatedBox(
                          quarterTurns: 3,
                          child: Text('LIONEL',
                              style: GoogleFonts.alfaSlabOne(
                                  color: ColorHelper.grey, fontSize: 45))),
                    ],
                  ),
                  Expanded(child: Container())
                ],
              ),
            ),
            Expanded(
                child: Container(
              padding: EdgeInsets.only(
                  left: Get.width * 0.03,
                  right: Get.width * 0.03,
                  top: Get.width * 0.03),
              decoration: BoxDecoration(
                  color: ColorHelper.dark,
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                  )),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: Get.width * 0.05),
                        child: Icon(Icons.sports_soccer,
                            color: ColorHelper.white, size: 35),
                      ),
                      Text('FORWARD',
                          style: GoogleFonts.alfaSlabOne(
                              color: ColorHelper.white,
                              letterSpacing: 2,
                              fontSize: 16))
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: Get.height * 0.03),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        StatisticBox(header: 'AGE', value: '34'),
                        StatisticBox(header: 'GAMES', value: '4'),
                        StatisticBox(header: 'GOALS', value: '3')
                      ],
                    ),
                  )
                ],
              ),
            ))
          ],
        ),
        Positioned(
          right: -30,
            child: Image.asset('assets/images/messi_2.png',
                height: Get.height * 0.60))
      ],
    );
  }
}
