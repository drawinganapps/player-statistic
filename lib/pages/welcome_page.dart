import 'package:fifa_worldcup_qatar/helper/color_helper.dart';
import 'package:fifa_worldcup_qatar/helper/dummy.dart';
import 'package:fifa_worldcup_qatar/routes/AppRoutes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: Get.width * 0.08, right: Get.width * 0.08),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset('assets/images/logo.svg', height: Get.height * 0.4),
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.03, bottom: Get.height * 0.03),
            child: Text(DummyData.intro,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: ColorHelper.dark.withOpacity(0.4),
                    height: 1.5,
                    fontSize: 18)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 15,
                width: 5,
                decoration: BoxDecoration(
                    color: ColorHelper.primary,
                    borderRadius: BorderRadius.circular(10)),
              ),
              Container(
                margin: const EdgeInsets.only(left: 5, right: 5),
                height: 10,
                width: 5,
                decoration: BoxDecoration(
                    color: ColorHelper.grey,
                    borderRadius: BorderRadius.circular(10)),
              ),
              Container(
                height: 10,
                width: 5,
                decoration: BoxDecoration(
                    color: ColorHelper.grey,
                    borderRadius: BorderRadius.circular(10)),
              ),
            ],
          ),
          GestureDetector(
            onTap: () {
              Get.toNamed(AppRoutes.PLAYER_DETAIL);
            },
            child: Container(
              margin: EdgeInsets.only(top: Get.height * 0.03),
              padding: EdgeInsets.only(
                  left: Get.width * 0.05,
                  right: Get.width * 0.05,
                  top: Get.width * 0.03,
                  bottom: Get.width * 0.03),
              decoration: BoxDecoration(
                  color: ColorHelper.primary,
                  borderRadius: BorderRadius.circular(30)),
              child: Text('GET STARTED',
                  style: GoogleFonts.alfaSlabOne(
                    color: ColorHelper.white,
                  )),
            ),
          )
        ],
      ),
    );
  }
}
